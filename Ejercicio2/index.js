
const LISTA = document.querySelector('#lista_codigo');
const LISTA2 = document.querySelector('#lista_cuidad');
const LISTA3 = document.querySelector('#lista_estado');
const LISTA4 = document.querySelector('#lista_tienda');
const URL = 'https://aeromexico.com/cms/api/v1/geoinfo?country=MX&status=1';

const REQUEST = new XMLHttpRequest();
REQUEST.open('GET', URL);

REQUEST.responseType = 'json';
REQUEST.send();

//funcion que imprime el codigo de areopuerto del json
function populateBody(jsonOBJ){

	for(let i =0; i<jsonOBJ.length; i++){

	 const MyLi = document.createElement('li');
	 MyLi.textContent = JSON.stringify(jsonOBJ[i].airportCode);
	 LISTA.appendChild(MyLi);


 }

}

//funcion que imprime la cuidad del json
function populateBody2(jsonOBJ){

	for(let i =0; i<jsonOBJ.length; i++){

	 const MyLi2 = document.createElement('li');
	 MyLi2.textContent = JSON.stringify(jsonOBJ[i].city);
	 LISTA2.appendChild(MyLi2);


 }

}

//funcion que imprime el estado del json
function populateBody3(jsonOBJ){

	for(let i =0; i<jsonOBJ.length; i++){

	 const MyLi3= document.createElement('li');
	 MyLi3.textContent = JSON.stringify(jsonOBJ[i].state);
	 LISTA3.appendChild(MyLi3);


 }

}

function populateBody4(jsonOBJ){

	for(let i =0; i<jsonOBJ.length; i++){

	 const MyLi4= document.createElement('li');
	 MyLi4.textContent = JSON.stringify(jsonOBJ[i].store);
	 LISTA4.appendChild(MyLi4);


 }

}


REQUEST.onload = function() {
  const INFO= REQUEST.response;
  populateBody(INFO);
  populateBody2(INFO);
  populateBody3(INFO);
  populateBody4(INFO);
}